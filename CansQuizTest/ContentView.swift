//
//  ContentView.swift
//  CansQuizTest
//
//  Created by Kuntawat surin on 9/12/2566 BE.
//

import SwiftUI

struct ContentView: View {
    
    @State var name: String = ""
    @State var output: String = ""
    
    var body: some View {
        ZStack {
            Color.white.ignoresSafeArea()
            VStack {
                Image("logo-cans")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 100)
                    .padding(.bottom, 40)
                
                //text input
                TextFieldLabelView(output: $output, labelName: "Input your phone number:", placeHolder: "Enter your phone number", phoneNumber: $name)
                    .padding()
                
                // output
                Text("Phone number: \(output)")
                    .foregroundColor(.black)
                    .font(.title3)
                    .padding()
                    .opacity(output.isEmpty ? 0 : 1)
                
                Spacer()
            }
            
        }
        .onTapGesture {
            hideKeyboard()
        }
    }
    
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

#Preview {
    ContentView()
}
