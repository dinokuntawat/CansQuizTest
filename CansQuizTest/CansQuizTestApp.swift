//
//  CansQuizTestApp.swift
//  CansQuizTest
//
//  Created by Kuntawat surin on 9/12/2566 BE.
//

import SwiftUI

@main
struct CansQuizTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
